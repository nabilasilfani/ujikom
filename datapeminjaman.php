<?php 
include "koneksi.php";
include "kode_peminjaman.php";
session_start();
if (isset($_SESSION['username'])){
  ?>

  <?php 
    $kode_peminjaman = $_GET['kode_peminjaman'];
   ?>
  <!DOCTYPE html>
  <html>
  <head>
    <title>Admin</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="style.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
   
  
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/highcharts.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script src="plugin/datatables/jquery.dataTables.js" type="text/javascript"></script>
  <script src="plugin/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
  
  <script type="text/javascript">
      $(function () {
        $('#table').dataTable({
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": false,
          "bInfo": true,
          "bAutoWidth": true
        });
      });
    </script>


</head>
<body>
 <nav>
  <div class="container">
    <div class="nav-wrapper">
      <div class="brand-logo">INVENSCOO</div>
      <ul class="right hide-on-med-and-down">
        <li><a href="logout2.php" style="color: white;"><i class="material-icons prefix">exit_to_app</i></a></li>
      </ul>
    </div>
  </div>
</nav>

<!-- Navbar goes here -->
  <!-- Page Layout here -->
  <div class="row">
<div class="container">
    <div class="col s3">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header" style="color:#039be5;"><a href="index.php"><i class="material-icons">dashboard</i>Dashboard</a></div>
        </li>
        <li>
          <div class="collapsible-header"><i class="material-icons">description</i>Data Master</div>
          <div class="collapsible-body"><a href="barang/databarang.php">Data Barang</a></div>
          <div class="collapsible-body"><a href="jenis/datajenis.php">Data Jenis</a></div>
          <div class="collapsible-body"><a href="ruang/dataruang.php">Data Ruang</a></div>
        </li>
   <li>
          <div class="collapsible-header"><i class="material-icons">people</i>Data Pengguna</div>
          <div class="collapsible-body"><a href="petugas/petugas.php">Data Petugas</a></div>
          <div class="collapsible-body"><a href="pegawai/pegawai.php">Data Pegawai</a></div>
       
        </li>
           <li>
          <div class="collapsible-header"><i class="material-icons">content_paste</i>Data Master</div>
          <div class="collapsible-body"><a href="datapeminjaman.php?kode_peminjaman=<?php echo $kode; ?>">Data Peminjaman</a></div>
          <div class="collapsible-body"><a href="datapengembalian.php">Data pengembalian</a></div>
          </li>
        </ul>
      </div>

    <div class="col s9"> 
      <div class="row">
        <?php 
        $sql = mysql_query("SELECT * FROM inventaris");
        while($data = mysql_fetch_array($sql)){
      ?>
    <div class="col s12 m6">
      <div class="card">
        <div class="card-image">
          <img src="../<?php echo $data['gambar'] ?>">
          <span class="card-title"><?php echo $data['nama'] ?></span>
        </div>
        <div class="card-action">
          <h6>Jumlah Barang :<?php echo $data['jumlah'] ?></h6>
          <a class="waves-effect waves-light modal-trigger" href="#modal1-<?php echo $data['id_inventaris'] ?>">Silahkan Pinjam</a>
        </div>
      </div>
    </div>
    <!-- Modal Structure -->
  <div id="modal1-<?php echo $data['id_inventaris'] ?>" class="modal">
    <div class="modal-content">
      <h4>Anda Akan Meminjam <?php echo $data['nama']; ?></h4>
      <br>
    <div class="card">
      <div class="card-content">
        <form action="" method="POST">
          <label>Nama Peminjam</label>
          <input type="text" name="nama" placeholder="Masukkan Nama"/><br>          
          <label>Barang</label>
          <input type="hidden" name="id_inventaris" value="<?php echo $data['id_inventaris'] ?>" readonly/><br>      
          <input type="text" name="barang" value="<?php echo $data['nama'] ?>" readonly/><br>
          <label>Kode Inventaris</label>
            <input type="text" name="kode_inventaris" value="<?php echo $data['kode_inventaris'] ?>" readonly/><br>
          <label>Kondisi</label>
          <input type="text" name="kondisi" value="<?php echo $data['kondisi'] ?>"  readonly/>
          <label>Kode Peminjaman</label>
          <input type="text" name="kode_peminjaman" value="<?php echo $kode_peminjaman ?>"  readonly/>
          <label>Jumlah</label>
          <input type="number" name="jumlah" min="1" max="<?php echo $data['jumlah'] ?>" value="1" placeholder="Masukkan Jumlah"/><br>
      
    </div>
  </div>
    </div>
    <div class="modal-footer">
      <input type="submit" class="btn btn-primary" name="send">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
  </div>
  </form>
    <?php 
    }
    ?>
    <?php
        include"koneksi.php";
        if(isset($_POST['send']))
        {

          $nama = $_POST['nama'];
          $barang = $_POST['barang'];
          $kondisi = $_POST['kondisi'];
          $jumlah = $_POST['jumlah'];
          $tanggal_peminjaman = date("Y-m-d");
          $kode_inventaris = $_POST['kode_inventaris'];
          $id_inventaris = $_POST['id_inventaris'];
          $kode_peminjaman = $_POST['kode_peminjaman'];
          $status = 'Pinjam';
          $input = mysql_query("INSERT INTO detail_pinjam (id_inventaris,jumlah,kode_peminjaman) VALUES ('$id_inventaris','$jumlah','$kode_peminjaman')");
          $input2 = mysql_query("INSERT INTO peminjaman (tanggal_pinjam,status_peminjaman,id_inventaris,kode_peminjaman1) VALUES ('$tanggal_peminjaman','$status','$id_inventaris','$kode_peminjaman')");
          $update_i = mysql_query("UPDATE inventaris SET jumlah = jumlah-$jumlah WHERE id_inventaris='$id_inventaris'");
          if($input){
            echo"<script>window.location.assign('datapengembalian.php')</script>";
          }else{
            echo"Gagal";
          }
        }
        ?>
  </div>
  
    </div>
</div>
</div>
 <footer class="page-footer">
  <div class="container">
    <div class="row">
      <div class="col l4 s12" style="width: 32.333333%; font-size: 16px;">
        <h5 class="white-text">Hubungi Kami</h5>
        <i class="material-icons" style="width: 24px; color: white;">add_locations</i><a style="color:white">SMK Negri 1 Ciomas | Jl.Laladon, Desa Laladon, Kecamatan Ciomas, Kab.Bogor.</a><br>
        <p><i class="material-icons" style="width: 27px; color: white">website</i><a style="color:white">www.smkn1ciomas.sch.id</a> </p>
        <p><i class="material-icons" style="margin-left: -95px; width: 123px; color: white">telephone</i><a style="color:white">08147919696594 </a></p>
      </div>

      <div class="col l4 s12" style="overflow: hidden; margin-left: 320px; font-size: 16px;">
        <h5 class="white-text">About</h5>
        <p class="grey-text text-lighten-4">Program ini memiliki fungsi untuk memudahkan siswa dalam pengabsenan dan memudahkan staf tata usaha dalam merekap absensi siswa.</p>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
      © SMK Negeri 1 CIOMAS
    </div>
  </div>
</footer>
<script type="text/javascript">
   document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, options);
  });

  // Or with jQuery

  $(document).ready(function(){
    $('.modal').modal();
  });
          
</script>
</body>
</html>
<?php
}else{
  header("location:login.php");
}
?>


 <script type="text/javascript">
        $(document).ready(function() {
          $('select').material_select();
        });
      </script> 