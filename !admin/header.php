<?php 
include "../../koneksi.php";
include "kode_peminjaman.php";
session_start();
if ($_SESSION['level']!='admin'){
  header("location:../../../login.php");
}
  ?>
  <!DOCTYPE html>
  <html>
  <head>
    <title>Admin</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="../../../css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="../../../style.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
   
  
  <script type="text/javascript" src="../../../js/jquery.js"></script>
  <script type="text/javascript" src="../../../js/highcharts.js"></script>
  <script type="text/javascript" src="../../../js/materialize.min.js"></script>
  <script src="../../../plugin/datatables/jquery.dataTables.js" type="text/javascript"></script>
  <script src="../../../plugin/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
  
  <script type="text/javascript">
      $(function () {
        $('#table').dataTable({
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": false,
          "bInfo": true,
          "bAutoWidth": true
        });
      });
    </script>
  </head>

  <body>
  <nav>
    <div class="container">
    <div class="nav-wrapper">
      <div class="brand-logo">INVENSCOO</div>
      <ul class="right hide-on-med-and-down">
        <li><a href="../../logout.php" style="color: white;"><i class="material-icons prefix">exit_to_app</i></a></li>
      </ul>
    </div>
  </div>
  </nav>
  <div class="container">
  <div class="row">
    <div class="col s3">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header" style="color:#039be5;"><a href="../../index.php"><i class="material-icons">dashboard</i>Dashboard</a></div>
        </li>
        <li>
          <div class="collapsible-header"><i class="material-icons">description</i>Data Master</div>
          <div class="collapsible-body"><a href="../barang/databarang.php">Data Barang</a></div>
          <div class="collapsible-body"><a href="../jenis/datajenis.php">Data Jenis</a></div>
          <div class="collapsible-body"><a href="../ruang/dataruang.php">Data Ruang</a></div>
        </li>
        <li>
            <div class="collapsible-header"><i class="material-icons">people</i>Data Pengguna</div>
            <div class="collapsible-body"><a href="../petugas/petugas.php">Data Petugas</a></div>
            <div class="collapsible-body"><a href="../pegawai/pegawai.php">Data Pegawai</a></div>
        </li>
        <li>
            <div class="collapsible-header"><i class="material-icons">content_paste</i>Data Master</div>
            <div class="collapsible-body"><a href="../../datapeminjaman.php?kode_peminjaman=<?php echo $kode; ?>">Data Peminjaman</a></div>
            <div class="collapsible-body"><a href="../../datapengembalian.php">Data pengembalian</a></div>
        </li>
      </ul>
    </div>

    <div class="col s9">
      <div class="card">