<?php 
include "koneksi.php";
include "kode_peminjaman.php";
session_start();
if (isset($_SESSION['username'])){
  ?>
  <!DOCTYPE html>
  <html>
  <head>
    <title>Admin</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="style.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>


    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/highcharts.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script src="plugin/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="plugin/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

    <script type="text/javascript">
      $(function () {
        $('#table').dataTable({
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": false,
          "bInfo": true,
          "bAutoWidth": true
        });
      });
    </script>


  </head>
  <body>
    <style>

    @media print{
      input.noprint{
        display:none;
      }
    }
  </style>
  <div class="container">
    <div class="row">

      <div class="col s12"> 
        <div class="card">
          <div class="card-heading"><center><h2>Data Pengembalian</h2></center></div>
          <div class="card-content">
            <div class="responsive-table">
              <table id="table" class="striped">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Tanggal Pinjam</th>
                    <th>Tanggal Kembali</th>
                    <th>Status Peminjaman</th>
                    <th>Id Pegawai</th>
                    <th>Kode Peminjaman</th>
                  </tr>
                </thead>
                <tbody>
                 <?php
                 include"koneksi.php";
                 $no=1;
                 $query=mysql_query("SELECT * FROM peminjaman LEFT JOIN inventaris ON peminjaman.id_inventaris = inventaris.id_inventaris LEFT JOIN detail_pinjam ON peminjaman.kode_peminjaman1=detail_pinjam.kode_peminjaman");
                 while ($tampil=mysql_fetch_array($query)) {
                  echo "<tr>
                  <td>$no</td>
                  <td>$tampil[nama]</td>
                  <td>$tampil[tanggal_pinjam]</td>
                  <td>$tampil[tanggal_kembali]</td>
                  <td>$tampil[status_peminjaman]</td>
                  <td>$tampil[id_pegawai]</td>                  
                  <td>$tampil[kode_peminjaman1]</td>
                  <td>
                  "?>
                  <?php 
                  if($tampil['status_peminjaman']=='Pinjam'){

                 }
                 echo"                  
                 </td>
                 </tr>";

                 $no++;
               }
               ?><tr><td>           <input type="button" value="Cetak Laporan" onclick="window.print()" class="noPrint">
</td></tr>
             </tbody>
           </table>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</body>
</html>
<?php
}else{
  header("location:login.php");
}
?>
