<?php 
include "koneksi.php";
include "kode_peminjaman.php";
session_start();
if (isset($_SESSION['username'])){
  ?>
  <!DOCTYPE html>
  <html>
  <head>
    <title>Admin</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="style.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
    
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/highcharts.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script>

    var chart1; 
    $(document).ready(function() {
     chart1 = new Highcharts.Chart({
       chart: {
         renderTo: 'mygraph',
         type: 'column'
       },   
       title: {
         text: 'WEB INVENSCOO '
       },
       xAxis: {
         categories: ['Peminjaman']
       },
       yAxis: {
         title: {
          text: 'Rate'
        }
      },
      series:             
      [
      <?php 
      include "koneksi.php";
      $query = mysql_query("SELECT * FROM inventaris group by nama");
      while($t=mysql_fetch_array($query))
      {      
        $sql=mysql_query("SELECT COUNT(a.id_inventaris) as total FROM inventaris d JOIN peminjaman a ON d.id_inventaris=a.id_inventaris WHERE d.nama='$t[nama]'");
        $r=mysql_fetch_array($sql);
        
        ?>
        {
         name: '<?php echo $t['nama']; ?>',
         data: [<?php echo $r['total']; ?>]
       },
       <?php 
     }  ?>
     ]
   });
   }); 
 </script>

  </head>
  <body>
   <nav>
    <div class="container">
      <div class="nav-wrapper">
        <div class="brand-logo">INVENSCOO</div>
        <ul class="right hide-on-med-and-down">
          <li><a href="logout.php" style="color: white;"><i class="material-icons prefix">exit_to_app</i></a></li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Navbar goes here -->
  <!-- Page Layout here -->
  <div class="row">
<div class="container">
    <div class="col s3">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header" style="color:#039be5;"><a href="index.php"><i class="material-icons">dashboard</i>Dashboard</a></div>
        </li>
        <li>
          <div class="collapsible-header"><i class="material-icons">description</i>Data Master</div>
          <div class="collapsible-body"><a href="page/barang/databarang.php">Data Barang</a></div>
          <div class="collapsible-body"><a href="page/jenis/datajenis.php">Data Jenis</a></div>
          <div class="collapsible-body"><a href="page/ruang/dataruang.php">Data Ruang</a></div>
        </li>
        <li>
          <div class="collapsible-header"><i class="material-icons">people</i>Data Pengguna</div>
          <div class="collapsible-body"><a href="page/petugas/petugas.php">Data Petugas</a></div>
          <div class="collapsible-body"><a href="page/pegawai/pegawai.php">Data Pegawai</a></div>
          
        </li>
        <li>
          <div class="collapsible-header"><i class="material-icons">content_paste</i>Data Master</div>
          <div class="collapsible-body"><a href="datapeminjaman.php?kode_peminjaman=<?php echo $kode; ?>">Data Peminjaman</a></div>
          <div class="collapsible-body"><a href="datapengembalian.php">Data pengembalian</a></div>
        </li>
      </ul>
    </div>

    <div class="col s9" style="margin-top: 9px; box-shadow: 0px 0px 8px -2px;"> 
      <div id="mygraph">
        
      </div> 
    </div>
    
  </div>
</div>
  <footer class="page-footer">
    <div class="container">
      <div class="row">
        <div class="col l4 s12" style="width: 32.333333%; font-size: 16px;">
          <h5 class="white-text">Hubungi Kami</h5>
          <i class="material-icons" style="width: 24px; color: white;">add_locations</i><a style="color:white">SMK Negri 1 Ciomas | Jl.Laladon, Desa Laladon, Kecamatan Ciomas, Kab.Bogor.</a><br>
          <p><i class="material-icons" style="width: 27px; color: white">website</i><a style="color:white">www.smkn1ciomas.sch.id</a> </p>
          <p><i class="material-icons" style="margin-left: -95px; width: 123px; color: white">telephone</i><a style="color:white">08147919696594 </a></p>
        </div>

        <div class="col l4 s12" style="overflow: hidden; margin-left: 320px; font-size: 16px;">
          <h5 class="white-text">About</h5>
          <p class="grey-text text-lighten-4">Program ini memiliki fungsi untuk memudahkan siapapun dalam mengetahui informasi inventaris yang ada di sekolah dan memudahkan staf tata usaha dalam merekap inventaris yang ada di sekolah.</p>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        © SMK Negeri 1 CIOMAS
      </div>
    </div>
  </footer>

</body>
</html>
<?php
}else{
  header("location:../login.php");
}
?>
