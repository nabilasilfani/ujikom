<!DOCTYPE html>
<?php
include "../koneksi.php";
session_start();
if (isset($_SESSION['username'])){
	header("location:index_user.php");
}else{
	?><!DOCTYPE html>
	<html>
	<head>
		<!--Import Google Icon Font-->
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!--Import materialize.css-->
		<link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="screen,projection"/>

		<!--Let browser know website is optimized for mobile-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>
	<body>

		<form action="proseslogin2.php" method="POST" class="login-form">
			<div class="card-panel">
				<!-- <div class="row"> -->
					<div class="input-field col s12 center">
						<img src ="../img/logo.png" width="50%">
					</div>
					    			<div class="input-field col s12">
    				<i class="material-icons prefix">perm_identity</i>
    				<input id="username" type="text" name="username" class="validate">
    				<label for="username">Username</label>
    			</div>
    			<div class="input-field col s12">
            <i class="material-icons prefix">lock_outline</i>
            <input id="password" type="password" name="password" class="validate">
            <label for="password">Password</label>
          </div>
					<div class="input-field col s12" style="margin-top: -10px;">
						<select name="level" class="browser-default">
							<option value="" selected>PILIH AKSES</option>
							<option value="3">GURU</option>
							<option value="4">SISWA</option>
						</select>
					</div>
					<div class="input-field prefix">
						<input type="submit" name="submit" class="btn btn-block" value="Login">
					</div>
					<!-- </div> -->
				</div>
			</form>
			<!--Import jQuery before materialize.js-->
			<script type="text/javascript" src="js/jquery.js"></script>
			<script type="text/javascript" src="js/materialize.min.js"></script>
		</body>
		</html>
		<style type="text/css">
		html,
		body {
			height: 100%;
		}
		html {
			display: table;
			margin: auto;
		}
		body {
			display: table-cell;
			vertical-align: middle;
			background-image: url(img/body-bg.png);

		}
		.login-form{
			width: 300px;
		}
		.btn-block{
			width: 100%;
		}

	</style>
	<?php
}
?>


<script type="text/javascript">
	$(document).ready(function() {
		$('select').material_select();
	});
</script> 
