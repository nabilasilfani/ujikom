<?php 
	include "koneksi.php";
	include "kode_peminjaman.php";
	$id=$_GET['id'] ;
	$nama = mysql_query("SELECT * FROM ruang WHERE id_ruang='$id'");
	$r = mysql_fetch_array($nama);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Admin</title>
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
	<link rel="stylesheet" type="text/css" href="style.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/highcharts.js"></script>
	<script type="text/javascript" src="js/materialize.min.js"></script>
</head>
<body>
	<nav>
    	<div class="container">
	      	<div class="nav-wrapper">
	        	<div class="brand-logo">INVENSCOO</div>
	        	<ul class="right hide-on-med-and-down">
	          		<li><a href="logout2.php" style="color: white;"><i class="material-icons prefix">exit_to_app</i></a></li>
	        	</ul>
	      	</div>
    	</div>
  	</nav>
  	<div class="container">
	    <div class="row">
	      <div class="col s12 m4 l4">
	        <ul class="collapsible" data-collapsible="accordion">
	          <li>
	            <div class="collapsible-header" style="color:#039be5;"><a href="index.php"><i class="material-icons">dashboard</i>Dashboard</a></div>
	          </li>
	          <li>
	            <div class="collapsible-header"><i class="material-icons">content_paste</i>Data Master</div>
	            <div class="collapsible-body"><a href="databarang.php">Data Barang</a></div>
	            <div class="collapsible-body"><a href="datajenis.php">Data Jenis</a></div>
	            <div class="collapsible-body"><a href="dataruang.php">Data Ruang</a></div>
	          </li>
	          <li>
	            <div class="collapsible-header"><i class="material-icons">people</i>Data Pengguna</div>
	            <div class="collapsible-body"><a href="petugas.php">Data Petugas</a></div>
	            <div class="collapsible-body"><a href="pegawai.php">Data Pegawai</a></div>

	          </li>
	          <li>
	            <div class="collapsible-header"><i class="material-icons">content_paste</i>Data Master</div>
	            <div class="collapsible-body"><a href="datapeminjaman.php?kode_peminjaman=<?php echo $kode; ?>">Data Peminjaman</a></div>
	            <div class="collapsible-body"><a href="datapengembalian.php">Data pengembalian</a></div>
	          </li>
	        </ul>
	      </div>
		  	<div class="col s12 m8 l8">
			    <div class="card">
			      <div class="card-heading"><h5>Edit Data Ruang</h5></div>
			      <div class="card-content">
			      	<form action="" method="POST">
				        <div class="form-group">
				          <label>Nama ruang</label>
				          <input class="form-control" type="hidden" name="id_ruang" value="<?php echo $r['id_ruang'];?>">
				          <input  class="form-control" placeholder="Masukan Nama Ruang"  name="nama_ruang" value="<?php echo $r['nama_ruang'];?>">
				        </div>
				        <div class="form-group">
				          <label>Kode Ruang</label>
				          <input class="form-control" placeholder="Masukan Kode Ruang"  name="kode_ruang" value="<?php echo $r['kode_ruang'];?>">
				        </div>                                 
				        <div class="form-group">
				          <label>Keterangan</label>
				          <input class="form-control" placeholder="Masukan Keterangan" name="keterangan" value="<?php echo $r['keterangan'];?>">
				        </div>
				        <button  class="btn btn-primary" type="submit" name="simpan" class="btn btn-default">Simpan</button>
			        </form>
					<?php
					if(isset($_POST['simpan'])){

					  $id_ruang=$_POST['id_ruang'];
					  $nama_ruang=$_POST['nama_ruang'];
					  $kode_ruang = $_POST['kode_ruang'];
					  $keterangan = $_POST['keterangan'];
					  $sql=mysql_query("UPDATE ruang set nama_ruang='$nama_raung',kode_ruang='$kode_ruang',keterangan='$keterangan' where id_ruang='$id'");
					  	if($sql){
					   		echo "<script>window.location.assign('dataruang.php')</script>";
						}else{
							echo"GAGAL";
						}
					}
					?>
			      </div>
			    </div>
			</div>
		</div>
	</div>
	<footer class="page-footer">
	  <div class="container">
	    <div class="row">
	      <div class="col l4 s12" style="width: 32.333333%; font-size: 16px;">
	        <h5 class="white-text">Hubungi Kami</h5>
	        <i class="material-icons" style="width: 24px; color: white;">add_locations</i><a style="color:white">SMK Negri 1 Ciomas | Jl.Laladon, Desa Laladon, Kecamatan Ciomas, Kab.Bogor.</a><br>
	        <p><i class="material-icons" style="width: 27px; color: white">website</i><a style="color:white">www.smkn1ciomas.sch.id</a> </p>
	        <p><i class="material-icons" style="margin-left: -95px; width: 123px; color: white">telephone</i><a style="color:white">08147919696594 </a></p>
	      </div>

	      <div class="col l4 s12" style="overflow: hidden; margin-left: 320px; font-size: 16px;">
	        <h5 class="white-text">About</h5>
	        <p class="grey-text text-lighten-4">Program ini memiliki fungsi untuk memudahkan siapapun dalam mengetahui informasi inventaris yang ada di sekolah dan memudahkan staf tata usaha dalam merekap inventaris yang ada di sekolah.</p>
	      </div>
	    </div>
	  </div>
	  <div class="footer-copyright">
	    <div class="container">
	      © SMK Negeri 1 CIOMAS
	    </div>
	  </div>
	</footer>
</body>
</html>