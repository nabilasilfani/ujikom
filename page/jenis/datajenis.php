<?php 
include "koneksi.php";
include "kode_peminjaman.php";
session_start();
if (isset($_SESSION['username'])){
  ?>
  <!DOCTYPE html>
  <html>
  <head>
    <title>Admin</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="style.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>


    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/highcharts.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script src="plugin/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="plugin/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

    <script type="text/javascript">
      $(function () {
        $('#table').dataTable({
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": false,
          "bInfo": true,
          "bAutoWidth": true
        });
      });
    </script>


  </head>
  <body>
   <nav>
    <div class="container">
      <div class="nav-wrapper">
        <div class="brand-logo">INVENSCOO</div>
        <ul class="right hide-on-med-and-down">
          <li><a href="logout2.php" style="color: white;"><i class="material-icons prefix">exit_to_app</i></a></li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Navbar goes here -->
    <!-- Page Layout here -->
    <div class="container">
    <div class="row">

      <div class="col s3">
        <ul class="collapsible" data-collapsible="accordion">
          <li>
            <div class="collapsible-header" style="color:#039be5;"><a href="index.php"><i class="material-icons">dashboard</i>Dashboard</a></div>
          </li>
          <li>
            <div class="collapsible-header"><i class="material-icons">description</i>Data Master</div>
            <div class="collapsible-body"><a href="databarang.php">Data Barang</a></div>
            <div class="collapsible-body"><a href="datajenis.php">Data Jenis</a></div>
            <div class="collapsible-body"><a href="dataruang.php">Data Ruang</a></div>
          </li>
          <li>
            <div class="collapsible-header"><i class="material-icons">people</i>Data Pengguna</div>
            <div class="collapsible-body"><a href="petugas.php">Data Petugas</a></div>
            <div class="collapsible-body"><a href="pegawai.php">Data Pegawai</a></div>

          </li>
          <li>
            <div class="collapsible-header"><i class="material-icons">content_paste</i>Data Master</div>
            <div class="collapsible-body"><a href="datapeminjaman.php?kode_peminjaman=<?php echo $kode; ?>">Data Peminjaman</a></div>
            <div class="collapsible-body"><a href="datapengembalian.php">Data pengembalian</a></div>
          </li>
        </ul>
      </div>
      <div class="col s9">
        <div class="card">
          <div class="card-heading"><h5>Data Jenis</h5></div>
          <div class="card-content">
            <div class="responsive-table">
              <table id="table" class="striped">
                <a class="waves-effect waves-light btn cyan modal-trigger" href="tamjen.php">Tambah</a>   
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Nama Jenis</th>
                    <th>Kode Jenis</th>
                    <th>Keterangan</th>
                    <th>Opsi</th>
                  </tr>
                </thead>
                <tbody>
                 <?php
                 $no=1;
                 $query=mysql_query("SELECT * FROM jenis");
                 while ($tampil=mysql_fetch_array($query)) {
                  echo "<tr>
                  <td>$no</td>
                  <td>$tampil[nama_jenis]</td>
                  <td>$tampil[kode_jenis]</td>
                  <td>$tampil[keterangan]</td>
                  <td>
                  <a href='edit_j.php?id=$tampil[id_jenis]'><button class='btn btn-floating blue'><i class='material-icons'>edit</i></a></button> 
                  <a href='hapus_jenis.php?id=$tampil[id_jenis]'><button class='btn btn-floating red','delete-material-icons'><i class='material-icons'>delete</i></a></button>
                  </td>
                  </tr>";

                  $no++;
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
<footer class="page-footer">
  <div class="container">
    <div class="row">
      <div class="col l4 s12" style="width: 32.333333%; font-size: 16px;">
        <h5 class="white-text">Hubungi Kami</h5>
        <i class="material-icons" style="width: 24px; color: white;">add_locations</i><a style="color:white">SMK Negri 1 Ciomas | Jl.Laladon, Desa Laladon, Kecamatan Ciomas, Kab.Bogor.</a><br>
        <p><i class="material-icons" style="width: 27px; color: white">website</i><a style="color:white">www.smkn1ciomas.sch.id</a> </p>
        <p><i class="material-icons" style="margin-left: -95px; width: 123px; color: white">telephone</i><a style="color:white">08147919696594 </a></p>
      </div>

      <div class="col l4 s12" style="overflow: hidden; margin-left: 320px; font-size: 16px;">
        <h5 class="white-text">About</h5>
        <p class="grey-text text-lighten-4">Program ini memiliki fungsi untuk memudahkan siswa dalam pengabsenan dan memudahkan staf tata usaha dalam merekap absensi siswa.</p>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
      © SMK Negeri 1 CIOMAS
    </div>
  </div>
</footer>

</body>
</html>
<?php
}else{
  header("location:login.php");
}
?>