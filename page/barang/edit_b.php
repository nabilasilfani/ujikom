<?php 
	include "koneksi.php";
	include "kode_peminjaman.php";
	$id=$_GET['id'] ;
	$nama = mysql_query("SELECT * FROM inventaris WHERE id_inventaris='$id'");
	$r = mysql_fetch_array($nama);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Admin</title>
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
	<link rel="stylesheet" type="text/css" href="style.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/highcharts.js"></script>
	<script type="text/javascript" src="js/materialize.min.js"></script>
</head>
<body>
	<nav>
    	<div class="container">
	      	<div class="nav-wrapper">
	        	<div class="brand-logo">INVENSCOO</div>
	        	<ul class="right hide-on-med-and-down">
	          		<li><a href="logout2.php" style="color: white;"><i class="material-icons prefix">exit_to_app</i></a></li>
	        	</ul>
	      	</div>
    	</div>
  	</nav>
  	<div class="container">
	    <div class="row">
	      <div class="col s12 m4 l4">
	        <ul class="collapsible" data-collapsible="accordion">
	          <li>
	            <div class="collapsible-header" style="color:#039be5;"><a href="index.php"><i class="material-icons">dashboard</i>Dashboard</a></div>
	          </li>
	          <li>
	            <div class="collapsible-header"><i class="material-icons">content_paste</i>Data Master</div>
	            <div class="collapsible-body"><a href="databarang.php">Data Barang</a></div>
	            <div class="collapsible-body"><a href="datajenis.php">Data Jenis</a></div>
	            <div class="collapsible-body"><a href="dataruang.php">Data Ruang</a></div>
	          </li>
	          <li>
	            <div class="collapsible-header"><i class="material-icons">people</i>Data Pengguna</div>
	            <div class="collapsible-body"><a href="petugas.php">Data Petugas</a></div>
	            <div class="collapsible-body"><a href="pegawai.php">Data Pegawai</a></div>

	          </li>
	          <li>
	            <div class="collapsible-header"><i class="material-icons">content_paste</i>Data Master</div>
	            <div class="collapsible-body"><a href=datapeminjaman.php?kode_peminjaman=<?php echo $kode; ?>">Data Peminjaman</a></div>
	            <div class="collapsible-body"><a href="datapengembalian.php">Data pengembalian</a></div>
	          </li>
	        </ul>
	      </div>
		  	<div class="col s12 m8 l8">
			    <div class="card">
			      <div class="card-heading"><h5>Edit Data Barang</h5></div>
			      <div class="card-content">
			      	<form action="" method="POST">
				        <div class="form-group">
				          <label>Nama</label>
				          <input class="form-control" type="hidden" name="id_inventaris" value="<?php echo $r['id_inventaris'];?>">
				          <input  class="form-control" placeholder="Masukan Nama"  name="nama" value="<?php echo $r['nama'];?>">
				        </div>
				        <div class="form-group">
				          <label>Kondisi</label>
				          <input class="form-control" placeholder="Masukan Kondisi"  name="kondisi" value="<?php echo $r['kondisi'];?>">
				        </div>                                 
				        <div class="form-group">
				          <label>Jumlah</label>
				          <input class="form-control" placeholder="Masukan Jumlah" name="jumlah" value="<?php echo $r['jumlah'];?>">
				        </div>
				        <div class="form-group">
				          <label>Tanggal Register</label>
				          <input class="form-control" placeholder="Masukan Tanggal Register" name="tanggal_register" value="<?php echo $r['tanggal_register'];?>">
				        </div>
				        <div class="form-group">
				          <label>Id Ruang</label>
				          <input class="form-control" placeholder="Masukan Id Ruang" name="id_ruang" value="<?php echo $r['id_ruang'];?>">
				        </div>
				        <div class="form-group">
				          <label>Kode Inventaris</label>
				          <input class="form-control" placeholder="Masukan Kode Inventaris" name="kode_inventaris" value="<?php echo $r['kode_inventaris'];?>">
				        </div>
				        <button  class="btn btn-primary" type="submit" name="simpan" class="btn btn-default">Simpan</button>
			        </form>
					<?php
					if(isset($_POST['simpan'])){

					  $id=$_POST['id_inventaris'];
					  $nama=$_POST['nama'];
					  $kondisi = $_POST['kondisi'];
					  $jumlah = $_POST['jumlah'];
					  $tanggal_register = $_POST['tanggal_register'];
					  $id_ruang = $_POST['id_ruang'];
					  $kode_inventaris = $_POST['kode_inventaris'];
					  $sql=mysql_query("UPDATE inventaris set nama='$nama',kondisi='$kondisi',jumlah='$jumlah',tanggal_register='$tanggal_register',id_ruang='$id_ruang',kode_inventaris='$kode_inventaris' where id_inventaris='$id'");
					  	if($sql){
					   		echo "<script>window.location.assign('databarang.php')</script>";
						}else{
							echo"GAGAL";
						}
					}
					?>
			      </div>
			    </div>
			</div>
		</div>
	</div>
	<footer class="page-footer">
	  <div class="container">
	    <div class="row">
	      <div class="col l4 s12" style="width: 32.333333%; font-size: 16px;">
	        <h5 class="white-text">Hubungi Kami</h5>
	        <i class="material-icons" style="width: 24px; color: white;">add_locations</i><a style="color:white">SMK Negri 1 Ciomas | Jl.Laladon, Desa Laladon, Kecamatan Ciomas, Kab.Bogor.</a><br>
	        <p><i class="material-icons" style="width: 27px; color: white">website</i><a style="color:white">www.smkn1ciomas.sch.id</a> </p>
	        <p><i class="material-icons" style="margin-left: -95px; width: 123px; color: white">telephone</i><a style="color:white">08147919696594 </a></p>
	      </div>

	      <div class="col l4 s12" style="overflow: hidden; margin-left: 320px; font-size: 16px;">
	        <h5 class="white-text">About</h5>
	        <p class="grey-text text-lighten-4">Program ini memiliki fungsi untuk memudahkan siapapun dalam mengetahui informasi inventaris yang ada di sekolah dan memudahkan staf tata usaha dalam merekap inventaris yang ada di sekolah.</p>
	      </div>
	    </div>
	  </div>
	  <div class="footer-copyright">
	    <div class="container">
	      © SMK Negeri 1 CIOMAS
	    </div>
	  </div>
	</footer>
</body>
</html>